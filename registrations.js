var faye = require('faye'),
    mysql = require('mysql'),
    options = require('./options.json');

var DB = options.db,
    DBHOST = options.dbhost,
    DBUSER = options.dbuser,
    DBPASS = options.dbpass;

var APP_PASSWORD = options.fayepass;

var db = mysql.createClient({
  host: DBHOST,
  database: DB,
  user: DBUSER,
  password: DBPASS
});

var moodleCourseID = options.moodleCourse,
    total = 0,
    ntotal;

var ClientAuth = {
  outgoing: function(message, callback) {
    message.ext = message.ext || {};
    message.ext.password = APP_PASSWORD;
    callback(message);
  }
};

var client = new faye.Client(options.fayeurl);
client.addExtension(ClientAuth);

function publishEnrolments() {
  db.query(
    'SELECT total FROM WEenrolments',
    function(err, results, fields) {
      if (err) {
        throw err;
      }
      console.log(results);
      if (results && results[0]) {
        ntotal = results[0].total;
        if (total !== ntotal) {
          db.query('SELECT count(distinct(user.country)) AS countries \
                    FROM user_enrolments JOIN user \
                    ON (user_enrolments.userid=user.id) \
                    WHERE user_enrolments.enrolid=' + moodleCourseID,
            function(err, results, fields) {
              var countries;
              if (err) {
                throw err;
              }
              console.log(results);
              countries = results[0].countries;
              total = ntotal;
              try {
                client.publish('/enrolments', {
                  total: total,
                  countries: countries
                });
                console.log(total, countries);
              } catch (err) {
                console.log('publish error', err);
              }
            }
          );
        }
      }
    }
  );
}

publishEnrolments();
setInterval(publishEnrolments, 5000);

