# CREATE TABLE WEenrolments (total UNSIGNED INTEGER DEFAULT 0);

delimiter //

CREATE TRIGGER ai_enrol AFTER INSERT ON user_enrolments
  FOR EACH ROW UPDATE WEenrolments SET total = total + 1
    WHERE NEW.enrolid = 999;

//

delimiter ;

